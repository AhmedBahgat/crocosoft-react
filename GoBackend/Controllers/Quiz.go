package Controllers

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"example.com/crocosoft/DBManager"
	"example.com/crocosoft/Models"
	"example.com/crocosoft/Utils"
	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func isQuizExisting(self *Models.Quiz) bool {
	collection := DBManager.SystemCollections.Quiz
	filter := bson.M{
		"title": self.Title,
	}
	_, results := Utils.FindByFilter(collection, filter)
	return len(results) > 0
}

func QuizGetById(id primitive.ObjectID) (Models.Quiz, error) {
	collection := DBManager.SystemCollections.Quiz
	filter := bson.M{"_id": id}
	var self Models.Quiz
	_, results := Utils.FindByFilter(collection, filter)
	if len(results) == 0 {
		return self, errors.New("obj not found")
	}
	bsonBytes, _ := bson.Marshal(results[0]) // Decode
	bson.Unmarshal(bsonBytes, &self)         // Encode
	return self, nil
}

func QuizModify(c *fiber.Ctx) error {
	collection := DBManager.SystemCollections.Quiz
	objID, _ := primitive.ObjectIDFromHex(c.Params("id"))

	_, results := Utils.FindByFilter(collection, bson.M{"_id": objID})
	if len(results) == 0 {
		c.Status(404)
		return errors.New("id is not found")
	}

	var self Models.Quiz
	c.BodyParser(&self)
	err := self.Validate()
	if err != nil {
		c.Status(500)
		return err
	}

	self.Modified = primitive.NewDateTimeFromTime(time.Now())
	updateData := bson.M{
		"$set": self.GetModifcationBSONObj(),
	}
	_, updateErr := collection.UpdateOne(context.Background(), bson.M{"_id": objID}, updateData)
	if updateErr != nil {
		c.Status(500)
		return errors.New("an error occurred when modifing Quizdocument")
	} else {
		c.Status(200).Send([]byte("Modified Successfully"))
		return nil
	}
}

func quizGetAll(self *Models.QuizSearch) ([]bson.M, error) {
	collection := DBManager.SystemCollections.Quiz
	var results []bson.M
	b, results := Utils.FindByFilter(collection, self.GetQuizSearchBSONObj())
	if !b {
		return results, errors.New("no quiz found")
	}
	return results, nil
}

func QuizGetAll(c *fiber.Ctx) error {
	var self Models.QuizSearch
	c.BodyParser(&self)

	results, err := quizGetAll(&self)
	if err != nil {
		c.Status(500)
		return err
	}

	response, _ := json.Marshal(bson.M{"result": results})
	c.Set("Content-Type", "application/json")
	c.Status(200).Send(response)
	return nil
}

func QuizNew(c *fiber.Ctx) error {
	collection := DBManager.SystemCollections.Quiz
	var self Models.Quiz
	c.BodyParser(&self)

	err := self.Validate()
	if err != nil {
		c.Status(500)
		return err
	}
	if isQuizExisting(&self) {
		return errors.New("quiz is already existing")
	}

	if len(self.QuestionsAnswers) == 0 {
		return errors.New("quiz MUST have at least one question")
	}

	self.Created = primitive.NewDateTimeFromTime(time.Now())
	self.Modified = primitive.NewDateTimeFromTime(time.Now())
	res, err := collection.InsertOne(context.Background(), self)
	if err != nil {
		c.Status(500)
		return err
	}
	response, _ := json.Marshal(res)
	c.Status(200).Send(response)
	return nil
}
