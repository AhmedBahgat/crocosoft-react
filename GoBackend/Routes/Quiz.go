package Routes

import (
	"example.com/crocosoft/Controllers"
	"github.com/gofiber/fiber/v2"
)

func QuizRoute(route fiber.Router) {
	route.Post("/new", Controllers.QuizNew)
	route.Put("/modify/:id", Controllers.QuizModify)
	route.Post("/get_all", Controllers.QuizGetAll)
}
