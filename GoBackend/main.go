package main

import (
	"fmt"

	"example.com/crocosoft/DBManager"
	"example.com/crocosoft/Routes"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/pprof"
)

func SetupRoutes(app *fiber.App) {
	Routes.QuizRoute(app.Group("/quiz"))
}

func main() {
	fmt.Println("Hello Crocosoft")
	fmt.Print("Initializing Database Connections ... ")
	initState := DBManager.InitalizeSystemCollections()
	if initState {
		fmt.Println("[OK]")
	} else {
		fmt.Println("[FAILED]")
		return
	}

	fmt.Print("Initializing the server ... ")

	app := fiber.New()
	app.Use(cors.New())
	app.Use(pprof.New())
	app.Static("/Public", "./Public")
	SetupRoutes(app)

	fmt.Println("[OK]")
	app.Listen(":10100")

}
