package Models

import (
	"fmt"
	"reflect"
	"strings"

	"example.com/crocosoft/Utils"
	validation "github.com/go-ozzo/ozzo-validation"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Quiz struct {
	ID               primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Title            string             `json:"title,omitempty"`
	Description      string             `json:"description,omitempty"`
	Url              string             `json:"url,omitempty"`
	Score            float64            `json:"score,omitempty"`
	Created          primitive.DateTime `json:"created,omitempty" bson:"created,omitempty"`
	Modified         primitive.DateTime `json:"modified,omitempty" bson:"modified,omitempty"`
	QuestionsAnswers []Question         `json:"questionsanswers"`
}
type Question struct {
	Id            int64    `json:"id,omitempty"`
	Text          string   `json:"text,omitempty"`
	FeedbackTrue  string   `json:"feedbacktrue,omitempty"`
	FeedbackFalse string   `json:"feedbackfalse,omitempty"`
	Answers       []Answer `json:"answers,omitempty"`
}

type Answer struct {
	Id     int64  `json:"id,omitempty"`
	IsTrue bool   `json:"istrue,omitempty"`
	Text   string `json:"text,omitempty"`
}

func (obj Quiz) GetIdString() string {
	return obj.ID.String()
}

func (obj Quiz) GetId() primitive.ObjectID {
	return obj.ID
}

func (obj Quiz) Validate() error {
	return validation.ValidateStruct(&obj,
		validation.Field(&obj.Title, validation.Required),
		validation.Field(&obj.Description, validation.Required),
		validation.Field(&obj.Url, validation.Required),
	)
}
func (obj Quiz) GetModifcationBSONObj() bson.M {
	self := bson.M{}
	valueOfObj := reflect.ValueOf(obj)
	typeOfObj := valueOfObj.Type()
	invalidFieldNames := []string{"ID"}

	for i := 0; i < valueOfObj.NumField(); i++ {
		if Utils.ArrayStringContains(invalidFieldNames, typeOfObj.Field(i).Name) {
			continue
		}
		self[strings.ToLower(typeOfObj.Field(i).Name)] = valueOfObj.Field(i).Interface()
	}
	return self
}

type QuizSearch struct {
	IDIsUsed          bool               `json:"idisused,omitempty" bson:"idisused,omitempty"`
	ID                primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	TitleIsUsed       bool               `json:"titleisused,omitempty"`
	Title             string             `json:"title,omitempty"`
	DescriptionIsUsed bool               `json:"descriptionisused,omitempty"`
	Description       string             `json:"description,omitempty"`
	UrlIsUsed         bool               `json:"urlisused,omitempty"`
	Url               string             `json:"url,omitempty"`
	ScoreIsUsed       bool               `json:"scoreisused,omitempty"`
	Score             float64            `json:"score,omitempty"`
	CreatedIsUsed     bool               `json:"createdisused,omitempty" bson:"createdisused,omitempty"`
	Created           primitive.DateTime `json:"created,omitempty" bson:"created,omitempty"`
	ModifiedIsUsed    bool               `json:"modifiedisused,omitempty" bson:"modifiedisused,omitempty"`
	Modified          primitive.DateTime `json:"modified,omitempty" bson:"modified,omitempty"`
}

func (obj QuizSearch) GetQuizSearchBSONObj() bson.M {
	self := bson.M{}
	if obj.IDIsUsed {
		self["_id"] = obj.ID
	}

	if obj.TitleIsUsed {
		regexPattern := fmt.Sprintf(".*%s.*", obj.Title)
		self["title"] = bson.D{{"$regex", primitive.Regex{Pattern: regexPattern, Options: "i"}}}
	}

	if obj.DescriptionIsUsed {
		regexPattern := fmt.Sprintf(".*%s.*", obj.Description)
		self["description"] = bson.D{{"$regex", primitive.Regex{Pattern: regexPattern, Options: "i"}}}
	}

	if obj.UrlIsUsed {
		regexPattern := fmt.Sprintf(".*%s.*", obj.Url)
		self["url"] = bson.D{{"$regex", primitive.Regex{Pattern: regexPattern, Options: "i"}}}
	}

	if obj.ScoreIsUsed {
		self["score"] = obj.Score
	}

	if obj.CreatedIsUsed {
		self["created"] = obj.Created
	}

	if obj.ModifiedIsUsed {
		self["modified"] = obj.Modified
	}

	return self
}
