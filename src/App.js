import QuizContainer from "./Views/QuizContainer";

function App() {
  return (
    <>
      <QuizContainer />
    </>
  );
}

export default App;
