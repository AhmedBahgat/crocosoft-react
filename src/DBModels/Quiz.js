const  DBModelQuizMainData = {
    _id: null,
    title: "",
    description: "",
    url: "",
    score: -1,
    created: null,
    modified: null,
}

const DBModelQuestion = {
    id: null,
    text: "",
    feedbacktrue: "",
    feedbackfalse: "",
    answers: [],
}

const  DBModelAnswer = {
    id: null,
    istrue: false,
    text: "",
}

export {DBModelQuizMainData, DBModelQuestion, DBModelAnswer}