import axios from "axios";
import { apiBaseUrl } from "../config";

const APIModelQuizAddNew = async(mainInfo, questions)=>{
    let apiData = {...mainInfo};
    apiData.questionsanswers = [...questions];

    console.log("apiData", apiData);
    try{
        let response = await axios({
            method: "post",
            url: `${apiBaseUrl}/quiz/new`,
            headers: {
            "Content-Type": "application/json",
            },
            data: apiData,
        });
        return [true, "Quiz has been added successfully"];
    }catch(error){
        return [false, error];
    }

}

const APIModelQuizModify = async(mainInfo, questions, quizID)=>{
    let apiData = {...mainInfo};
    apiData.questionsanswers = [];
    apiData.questionsanswers = [...questions];

    try{
        let response = await axios({
            method: "put",
            url: `${apiBaseUrl}/quiz/modify/${quizID}`,
            headers: {
            "Content-Type": "application/json",
            },
            data: apiData,
        });
        return [true,"Quiz has been added successfully"];
    }catch(error){
        return [false, error];
    }
}

const APIModelQuizGetAll = async(filter)=>{
    try{
        let response = await axios({
            method: "post",
            url: `${apiBaseUrl}/quiz/get_all`,
            headers: {
            "Content-Type": "application/json",
            },
            data: JSON.stringify(filter),
        });
        return response.data.result ? response.data.result : [];
    }catch(error){
        return "Failed to add new quiz"
    }
}

export {APIModelQuizAddNew, APIModelQuizModify, APIModelQuizGetAll};