import React, { useEffect, useState } from "react";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';
import InputAdornment from '@mui/material/InputAdornment';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import {DBModelQuizMainData, DBModelQuestion} from '../DBModels/Quiz';
import {APIModelQuizAddNew, APIModelQuizModify} from '../APIModels/Quiz';
import DiagManageAnswers from "./DiagManageAnswers";

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });

function ManageQuiz({ quizToBeEdited }) {
    const [viewMode, setViewMode] = useState(viewModesVIEW);
    const [quizMainData, setQuizMainData] = useState(DBModelQuizMainData);
    const [quizCurrentQuestion, setQuizCurrentQuestion] = useState(DBModelQuestion);
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(null);
    const [quizQuestions, setQuizQuestions] = useState([]);
    const [selfDisable, setSelfDisable] = useState(true);

    const [openAnswersDiag, setOpenAnswersDiag] = useState(false);
    const [userFeedback, setUserFeedBack] = useState({
        type: "success",
        msg: "",
        open: false,
    });

    const notifyUser = (obj) =>{
        setUserFeedBack({...obj});
    }

    const addTheCurrentQuestion = () =>{
        if(quizCurrentQuestion.text === "")
        {
            notifyUser({type: "error", msg: "missing question title", open: true});
            return;
        }

        if(quizCurrentQuestion.answers.length <= 0)
        {
            notifyUser({type: "error", msg: "at least one answer is required for a question", open: true});
            return;
        }

        //check if there is at least one correct answer
        let hasAtLeastOneCorrectAnswer = false;
        for(let i = 0; i < quizCurrentQuestion.answers.length; i++){
            if(quizCurrentQuestion.answers[i].istrue)
            {
                hasAtLeastOneCorrectAnswer = true;
                break;
            }
        }

        if(!hasAtLeastOneCorrectAnswer)
        {
            notifyUser({type: "error", msg: "the question MUST have at least one correct answer", open: true});
            return;
        }

        // add ned question
        if(currentQuestionIndex === null){
            setQuizQuestions([...quizQuestions, {...quizCurrentQuestion}]);
            setQuizCurrentQuestion(DBModelQuestion);
            notifyUser({type: "success", msg: "question added successfully", open: true});
        }
        else{
            // modify the selected one
            let newQuestionList = [...quizQuestions];
            newQuestionList[currentQuestionIndex] = {...quizCurrentQuestion};
            setCurrentQuestionIndex(null);
            setQuizCurrentQuestion(DBModelQuestion);
            setQuizQuestions([...newQuestionList]);
            notifyUser({type: "success", msg: "question modified successfully", open: true});
        }
    }

    const deleteQuestion = (questionIndex)=>{
        let newQuestionList = [];
        for(let i = 0; i < quizQuestions.length; i++)
        {
            if(i !== questionIndex)
            newQuestionList.push(quizQuestions[i]);
        }
        setQuizQuestions([...newQuestionList]);
    }

    const addNew = async()=>{
        let [isSuccess, auxillary] = await APIModelQuizAddNew(quizMainData, quizQuestions);
        if (isSuccess)
        {
            notifyUser({type: "success", msg: auxillary, open: true});
            clear();
            setViewMode(viewModesVIEW);
            return;
        }else{
            notifyUser({type: "error", msg: auxillary.response.data, open: true});
        }
    }

    const editQuiz= async()=>{
        let [isSuccess, auxillary] = await APIModelQuizModify(quizMainData, quizQuestions, quizMainData._id);
        if (isSuccess)
        {
            notifyUser({type: "success", msg: auxillary, open: true});
            clear();
            setViewMode(viewModesVIEW);
            return;
        }else{
            notifyUser({type: "error", msg: auxillary.response.data, open: true});
        }
    }

    const clear = ()=>{
        setCurrentQuestionIndex(null);
        setQuizCurrentQuestion(DBModelQuestion);
        setQuizMainData(DBModelQuizMainData);   
        setQuizQuestions([]);       
    }

    // constrols selfDisable state
    useEffect(()=>{
        setSelfDisable(viewMode !== viewModesNEW && viewMode !== viewModesEDIT);
    }, [viewMode]);

    // load the clicked quiz from the other component
    useEffect(()=>{
        if (quizToBeEdited !== null)
        {
            setViewMode(viewModesTOBEEDIT);
            setQuizMainData({
                _id: quizToBeEdited._id,
                title: quizToBeEdited.title,
                description: quizToBeEdited.description,
                url: quizToBeEdited.url,
            });

            setQuizQuestions([...quizToBeEdited.questionsanswers]);
        }
    }, [])

    return(
    <>
        <DiagManageAnswers open={openAnswersDiag} setOpen={setOpenAnswersDiag}
            question={quizCurrentQuestion} setQuestion={setQuizCurrentQuestion}
        />

        <Snackbar open={userFeedback.open} autoHideDuration={6000} onClose={(e)=>setUserFeedBack({...userFeedback, open: false})}>
            <Alert onClose={(e)=>setUserFeedBack({...userFeedback, open: false})} severity={userFeedback.type} sx={{ width: '100%' }}>
                {userFeedback.msg}
            </Alert>
        </Snackbar>

        {/*------------------- Control Buttons -----------------------*/}
        <Grid container style={styles.MainGrid}>
            <Grid item xs={12} >
                <Button variant="contained" color="success" size="small"
                style={styles.controlButtons}
                disabled={viewMode !== viewModesVIEW}
                onClick={(e) => {
                    clear();
                    setViewMode(viewModesNEW);
                }}
                >
                    Create New
                </Button>

                <Button variant="contained" color="secondary" size="small"
                style={styles.controlButtons}
                disabled={ viewMode !== viewModesTOBEEDIT }
                onClick={(e) => {
                    setViewMode(viewModesEDIT);
                }}
                >
                    Edit
                </Button>

                <Button variant="contained" color="primary" size="small"
                style={styles.controlButtons}
                disabled={viewMode !== viewModesNEW && viewMode !== viewModesEDIT}
                onClick={(e) => {
                    if (viewMode === viewModesNEW)
                        addNew();
                    else
                        editQuiz();
                }}
                >
                    Save
                </Button>

                <Button variant="contained" color="error" size="small"
                style={styles.controlButtons}
                onClick={(e) => {
                    clear();
                    setViewMode(viewModesVIEW);
                }}
                >
                    Cancel
                </Button>
            </Grid>
        </Grid>

        {/*------------------------ Quiz Main Data -------------------------*/}
        <Grid container style={styles.MainGrid} spacing={3}>
            <Grid item xs={9}>
                <TextField id="quiz-title" label="Title" variant="outlined"
                size="small" required fullWidth disabled={selfDisable}
                value={quizMainData.title} error={!quizMainData.title || quizMainData.title === ""}
                onChange={(e)=>{setQuizMainData({...quizMainData, title: e.target.value})}}
                />
            </Grid>

            <Grid item xs={3}>
                <Typography component="legend">Score</Typography>
                <Rating name="read-only" value={quizMainData.score ? quizMainData.score : 0} readOnly />
            </Grid>

            <Grid item xs={12}>
                <TextField id="quiz-desc" label="Description" variant="outlined"
                size="small" required fullWidth multiline minRows={3} disabled={selfDisable}
                value={quizMainData.description} error={!quizMainData.description || quizMainData.description === ""}
                onChange={(e)=>{setQuizMainData({...quizMainData, description: e.target.value})}}
                />
            </Grid>

            <Grid item xs={9}>
                <TextField id="quiz-url" label="YouTube Url" variant="outlined"
                size="small" required fullWidth disabled={selfDisable}
                value={quizMainData.url} error={!quizMainData.url || !quizMainData.url.startsWith("https://www.youtube.com/")}
                onChange={(e)=>{setQuizMainData({...quizMainData, url: e.target.value})}}
                />
            </Grid>
        </Grid>
        
        {/*---------------------------------- Question Input Area ---------------------------*/}
        <Divider />
        <Grid container item xs={12}>
            <Paper elevation={3} style={styles.QuestionPaper}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField id="q-text" label="Question Text" variant="outlined"
                        size="small" required fullWidth disabled={selfDisable}
                        value={quizCurrentQuestion.text}
                        onChange={(e)=>{setQuizCurrentQuestion({...quizCurrentQuestion, text: e.target.value})}}
                        />
                    </Grid>
                    <Grid item xs={10}>
                        <TextField id="q-text" label="True Feedback" variant="outlined"
                        size="small" fullWidth disabled={selfDisable}
                        InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <CheckIcon />
                              </InputAdornment>
                            ),
                        }}
                        value={quizCurrentQuestion.feedbacktrue}
                        onChange={(e)=>{setQuizCurrentQuestion({...quizCurrentQuestion, feedbacktrue: e.target.value})}}
                        />
                    </Grid>
                    <Grid item xs={10}>
                        <TextField id="q-text" label="False Feedback" variant="outlined"
                        size="small" fullWidth disabled={selfDisable}
                        InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <CloseIcon />
                              </InputAdornment>
                            ),
                        }}
                        value={quizCurrentQuestion.feedbackfalse}
                        onChange={(e)=>{setQuizCurrentQuestion({...quizCurrentQuestion, feedbackfalse: e.target.value})}}
                        />
                    </Grid>

                    <Grid container item xs={12} spacing={3}>
                            <Button variant="contained" color="primary" size="small"
                            style={{margin:20, }} disabled={selfDisable}
                            onClick={(e) => setOpenAnswersDiag(true)}
                            >
                                {`Manage answers (${quizCurrentQuestion.answers.length})`}
                            </Button>

                            <Button variant="contained" color="success" size="small"
                            style={{margin:20, }} disabled={selfDisable}
                            onClick={(e) => addTheCurrentQuestion()}
                            >
                                Add Question / Apply changes 
                            </Button>
                    </Grid>
                </Grid>
            </Paper>

            {/*---------------------------------- Questions Table ---------------------------*/}
            <Grid container item xs={12}>
                <TableContainer component={Paper}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow style={{backgroundColor:"#42a5f5"}}>
                                <TableCell >Actions</TableCell>
                                <TableCell><b>Question Text</b></TableCell>
                                <TableCell><b>true Feedback</b></TableCell>
                                <TableCell><b>false Feedback</b></TableCell>
                                <TableCell><b>No. of Answers</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {quizQuestions.map((row, index) => (
                                <TableRow key={index} >
                                    <TableCell component="th" scope="row">
                                        <Button variant="contained" color="primary" size="small"
                                        disabled={selfDisable}
                                        onClick={(e) => {
                                            setCurrentQuestionIndex(index);
                                            setQuizCurrentQuestion({...row});
                                        }}
                                        >
                                            <ModeEditIcon style={{fontSize: "small"}}/>
                                        </Button>

                                        <Button variant="contained" color="error" size="small"
                                        disabled={selfDisable}
                                        onClick={(e) => deleteQuestion(index)}
                                        >
                                            <DeleteForeverIcon style={{fontSize: "small"}}/>
                                        </Button>  
                                    </TableCell>
                                    <TableCell>{row.text}</TableCell>
                                    <TableCell>{row.feedbacktrue ? row.feedbacktrue : "N/A"}</TableCell>
                                    <TableCell>{row.feedbackfalse ? row.feedbackfalse : "N/A"}</TableCell>
                                    <TableCell>
                                        <Button variant="contained" color="secondary" size="small"
                                        disabled={selfDisable}
                                        onClick={(e)=>{
                                            setQuizCurrentQuestion({...row});
                                            setCurrentQuestionIndex(index);
                                            setOpenAnswersDiag(true);
                                        }}>
                                            {row.answers?.length}
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </Grid>

    </>
    );
}

export default ManageQuiz;

// Constants
const viewModesVIEW = "VIEW";
const viewModesNEW = "NEW";
const viewModesEDIT = "EDIT";
const viewModesTOBEEDIT = "ToBeEdited";

// Components CSS
const styles = {
    controlButtons : {
        marginRight : 10,
    },
    MainGrid: {
        padding: 20,
    },
    QuestionPaper:{
        padding: 20,
        width: "80%",
        marginLeft:"10%",
        marginRight:"10%",
        mariginTop: 20,
        marginBottom: 10,
    }
}