import React, { useEffect, useState } from "react";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import SearchIcon from '@mui/icons-material/Search';
import ModeEditIcon from '@mui/icons-material/ModeEdit';

import {APIModelQuizGetAll} from '../APIModels/Quiz';

function ViewAllQuizes({ setTabIndex, setQuizToBeEdited}){
    const [allQuizes, setAllQuizes] = useState([]);
    const [filter, setFilter] = useState({
        title : "",
        description: "",
        descriptionisused: false,
        titleisused: false,
    });

    const search = async()=>{
        console.log(filter);
        let apiQuizes = await APIModelQuizGetAll(filter);
        if (typeof(apiQuizes) !== 'string')
            setAllQuizes(apiQuizes);
    }

    const handleYoutubeClick = (url) => {
        window.open(url);
    };

    useEffect(()=>{
        search();
    }, []);

    return(
        <>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <TextField id="quiz-title" label="Search by Title" variant="outlined"
                    size="small" fullWidth
                    value={filter.title}
                    onChange={(e)=>{
                        if (e.target.value !== "")
                            setFilter({...filter, title: e.target.value, titleisused: true});
                        else
                            setFilter({...filter, title: e.target.value, titleisused: false});
                    }}
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField id="quiz-desc" label="Search by Description" variant="outlined"
                    size="small" fullWidth
                    value={filter.description}
                    onChange={(e)=>{
                        if (e.target.value !== "")
                            setFilter({...filter, description: e.target.value, descriptionisused: true});
                        else
                            setFilter({...filter, description: e.target.value, descriptionisused: false});
                    }}
                    />
                </Grid>
                <Grid item xs={2}>
                    <Button variant="contained" color="primary" size="small" onClick={(e)=>search()}>
                        <SearchIcon style={{fontSize:"meduim"}}/>
                    </Button>
                </Grid>
            </Grid>

            <Divider style={{margin:30}}/>

            <Grid container item xs={12}>
                <TableContainer component={Paper}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow style={{backgroundColor:"#42a5f5"}}>
                                <TableCell >Actions</TableCell>
                                <TableCell><b>Quiz Title</b></TableCell>
                                <TableCell><b>Description</b></TableCell>
                                <TableCell><b>Youtube</b></TableCell>
                                <TableCell><b>No. of Questions</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {allQuizes.map((row, index) => (
                                <TableRow key={index} >
                                <TableCell component="th" scope="row">
                                    <Button variant="contained" color="primary" size="small"
                                    onClick={(e) => {
                                        setTabIndex(1); // switch to the details view
                                        setQuizToBeEdited(row); // send the target quiz to view details 
                                    }}
                                    >
                                        <ModeEditIcon style={{fontSize: "small"}}/>
                                    </Button>
                                </TableCell>
                                <TableCell>{row.title}</TableCell>
                                <TableCell>{row.description?.length > 30 ? row.description.substring(0,30) + "..." :row.description}</TableCell>
                                <TableCell>
                                    <Button variant="contained" color="error" size="small"
                                    onClick={(e)=>handleYoutubeClick(row.url)}
                                    >
                                        Youtube
                                    </Button>
                                </TableCell>
                                <TableCell>{row.questionsanswers.length}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </>
    );
}

export default ViewAllQuizes;