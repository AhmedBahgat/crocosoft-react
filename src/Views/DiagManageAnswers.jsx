import React, { useState } from "react";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Chip from '@mui/material/Chip';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';

import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import {DBModelAnswer} from '../DBModels/Quiz';

function DiagManageAnswers({open, setOpen, question, setQuestion}){
    const [answer, setAnswer] = useState(DBModelAnswer);

    const deleteAnswer = (ansIndex)=>{
        let newAnswerList = [];
        for(let i = 0; i < question.answers.length; i++)
        {
            if(i !== ansIndex)
                newAnswerList.push(question.answers[i]);
        }
        setQuestion({...question, answers:[...newAnswerList]});
    }

    const addTheCurrentAnswer = ()=>{
        let newAnswerList = [...question.answers];
        newAnswerList.push({...answer});
        setQuestion({...question, answers: newAnswerList});
        setAnswer(DBModelAnswer);
    }

    return(
        <Dialog fullWidth maxWidth='md' open={open} onClose={(e)=>setOpen(false)} >
            <DialogTitle>Answers</DialogTitle>

            <DialogContent>
                <Grid container spacing={3}>
                    <Grid item xs={8}>
                        <TextField id="quiz-title" label="Answer text" variant="outlined"
                        size="small" required fullWidth
                        value={answer.text} error={!answer.text || answer.text === ""}
                        onChange={(e)=>{setAnswer({...answer, text: e.target.value})}}
                        />
                    </Grid>

                    <Grid item xs={2}>
                        <FormControlLabel label="is true ?" 
                        control={
                            <Switch defaultChecked checked={answer.istrue} onChange={(e)=>setAnswer({...answer, istrue: e.target.checked})} />
                        }  
                        />  
                    </Grid>

                    <Grid item xs={1}>
                        <Button variant="contained" color="success" size="meduim"
                            onClick={(e) => addTheCurrentAnswer()}
                            disabled={answer.text === ""}
                            >
                                +
                        </Button>
                    </Grid>
                </Grid>
                </DialogContent>
                <DialogContent>
                {/*-----------------answers table------------------*/}
                <Grid container spacing={3}>
                    <TableContainer component={Paper}>
                        <Table size="small" aria-label="a dense table">
                            <TableHead>
                                <TableRow style={{backgroundColor:"#42a5f5"}}>
                                    <TableCell>Actions</TableCell>
                                    <TableCell><b>Answer Text</b></TableCell>
                                    <TableCell><b>Is true</b></TableCell>
                                </TableRow>
                            </TableHead>

                            <TableBody>
                                {question.answers?.map((row, index) => (
                                    <TableRow key={index}>
                                        <TableCell component="th" scope="row">
                                            <Button variant="contained" color="error" size="small"
                                                style={{margin:20, }}
                                                onClick={(e) => deleteAnswer(index)}
                                                >
                                                    <DeleteForeverIcon style={{fontSize: "small"}}/>
                                            </Button>
                                        </TableCell>
                                        <TableCell>{row.text}</TableCell>
                                        <TableCell><Chip label={row.istrue ? "yes" : "no"} color={row.istrue ? "success" : "error"} /></TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </DialogContent>
        </Dialog>

    );
}

export default DiagManageAnswers;