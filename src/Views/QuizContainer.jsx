import React, { useState } from "react";

import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import ManageQuiz from "./ManageQuiz";
import ViewAllQuizes from "./ViewAllQuizes";
function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
}
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};
  
function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
}

function QuizContainer() {
    const [tabIndex, setTabIndex] = useState(0);
    const [quizToBeEdited, setQuizToBeEdited] = useState(null);
  
    const handleChange = (event, newValue) => {
        setTabIndex(newValue);
    };
  
    return (
        <Box sx={{ width: '100%' }}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={tabIndex} onChange={handleChange} aria-label="basic tabs example">
              <Tab label="All Quizes" {...a11yProps(0)} />
              <Tab label="Add/Edit Quiz" {...a11yProps(1)} />
            </Tabs>
          </Box>
          <TabPanel value={tabIndex} index={0}>
            <ViewAllQuizes setTabIndex={setTabIndex} setQuizToBeEdited={setQuizToBeEdited}/>
          </TabPanel>
          <TabPanel value={tabIndex} index={1}>
            <ManageQuiz quizToBeEdited={quizToBeEdited}/>
          </TabPanel>
        </Box>
      );
}

export default QuizContainer;